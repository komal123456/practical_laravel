<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('/frontend/assets/bootstrap/js/bootstrap.min.js') }}" defer></script> -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->

    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="//fonts.gstatic.com"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->

    <!-- Styles -->
    <!-- <link src="{{ asset('/frontend/assets/bootstrap/css/bootstrap.min.css') }}" defer></link> -->
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="{{ asset('/backend/assets/fonts/fontawesome/css/fontawesome-all.min.css') }}">
    <!-- animation css -->
    <link rel="stylesheet" href="{{ asset('/backend/assets/plugins/animation/css/animate.min.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ asset('/backend/assets/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/assets/css/rowReorder.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/backend/assets/css/jquery-confirm.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/backend/assets/css/jquery.loadingModal.css') }}" rel="stylesheet" media="all">

    <link href="{{ asset('/backend/assets/css/sweetalert2.min.css') }}" rel="stylesheet" media="all">

    <link href="{{ asset('/backend/assets/css/select2.min.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
        .application_form .add_btn span.fa-plus-circle, .application_form .remove_btn span.fa-minus-circle{
            display: inline-block;
            transition: color 150ms linear, background 150ms linear, font-size 150ms linear, width 150ms;
            font-size: 25px;
            color: #1a1a1a;
        }
        .invalid-feedback{
            display:block;
        }
    </style>

</head>
<body>
    <div id="app">
        

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>

<!-- Scripts -->
<script src="{{ asset('/backend/assets/js/vendor-all.min.js') }}"></script>
    <script src="{{ asset('/backend/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/backend/assets/js/pcoded.min.js') }}"></script>
    

    <script type="text/javascript" src="{{ asset('/backend/assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/backend/assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/backend/assets/js/dataTables.checkboxes.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/backend/assets/js/dataTables.rowReorder.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/backend/assets/js/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/backend/assets/js/jquery.validate.file.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/backend/assets/js/jquery-confirm.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/backend/assets/js/jquery.loadingModal.js') }}"></script>

    <script  src="{{ asset('/backend/assets/js/sweetalert2.all.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/backend/assets/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/backend/assets/js/selects/bootstrap_multiselect.js') }}"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js"></script>

    <script src="{{ asset('/backend/assets/js/custom.js') }}"></script>

    @stack('js_link')

    @stack('js')

    