
@extends('layouts.app')

@section('content')
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->

                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <div class="row">

                          <div class="col-md-10 mx-auto">
                            @include('admin.layout.session')
                          </div>

                          <div class="col-md-10 mx-auto text-center pb-5 pt-5">
                            <h2>Application Form</h2>
                          </div>


                          <div class="col-md-10 mx-auto">
                              <form action="{{route('applications.store')}}" method="post" class="application_form">
                                  @csrf
                                  <div class="form-group row">
                                    <div class="col-sm-12"><h2>Basic Details</h2></div>
                                      <div class="col-sm-6">
                                          <label>Name</label>
                                          <input type="text" name="name" value="{{old('name')}}" class="form-control" placeholder="Name" required>
                                          @if ($errors->has('name'))
                                              <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-6">
                                          <label>Email</label>
                                          <input type="email" name="email" value="{{old('email')}}" class="form-control"  placeholder="Email" required>
                                          @if($errors->has('email'))
                                              <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <div class="col-sm-6">
                                          <label>Address</label>
                                          <input type="text" name="address" value="{{old('address')}}" class="form-control"  placeholder="Address" required>
                                          @if ($errors->has('address'))
                                              <div class="invalid-feedback">{{ $errors->first('address') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-6">
                                          <label>Gender</label>
                                          <div class="form-group">
                                            <div class="form-check form-check-inline">
                                              <input class="form-check-input" checked type="radio" name="gender" id="inlineRadio1" value="male" required>
                                              <label class="form-check-label" for="inlineRadio1">Male</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                              <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="female" required>
                                              <label class="form-check-label" for="inlineRadio2">Female</label>
                                            </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-sm-12"><h2>Education Details</h2></div>
                                      <div class="col-sm-4">
                                          <label>SSC Board/University</label>
                                          <input type="text" name="ssc_university" value="{{old('ssc_university')}}" class="form-control" placeholder="SSC Board/University" required>
                                          @if ($errors->has('ssc_university'))
                                              <div class="invalid-feedback">{{ $errors->first('ssc_university') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-4">
                                          <label>SSC Year</label>
                                          <input type="text" name="ssc_year" value="{{old('ssc_year')}}" min="1900" max="2021" step="1" class="form-control"  placeholder="Enter SSC year" required>
                                          @if ($errors->has('ssc_year'))
                                              <div class="invalid-feedback">{{ $errors->first('ssc_year') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-4">
                                          <label>SSC CGPA/Percentage</label>
                                          <input type="text" name="ssc_percentage" value="{{old('ssc_percentage')}}" class="form-control"  placeholder="Enter SSC CGPA/Percentage" required>
                                          @if ($errors->has('ssc_percentage'))
                                              <div class="invalid-feedback">{{ $errors->first('ssc_percentage') }}</div>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <div class="col-sm-4">
                                          <label>HSC Board/University</label>
                                          <input type="text" name="hsc_university" value="{{old('hsc_university')}}" class="form-control" placeholder="HSC Board/University" required>
                                          @if ($errors->has('hsc_university'))
                                              <div class="invalid-feedback">{{ $errors->first('hsc_university') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-4">
                                          <label>HSC Year</label>
                                          <input type="text" name="hsc_year" value="{{old('hsc_year')}}" min="1900" max="2021" step="1" class="form-control"  placeholder="Enter HSC year" required>
                                          @if ($errors->has('hsc_year'))
                                              <div class="invalid-feedback">{{ $errors->first('hsc_year') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-4">
                                          <label>HSC CGPA/Percentage</label>
                                          <input type="text" name="hsc_percentage" value="{{old('hsc_percentage')}}" class="form-control"  placeholder="Enter HSC CGPA/Percentage" required>
                                          @if ($errors->has('hsc_percentage'))
                                              <div class="invalid-feedback">{{ $errors->first('hsc_percentage') }}</div>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <div class="col-sm-4">
                                          <label>Graduation Board/University</label>
                                          <input type="text" name="graduation_university" value="{{old('graduation_university')}}" class="form-control" placeholder="Graduation Board/University" required>
                                          @if ($errors->has('graduation_university'))
                                              <div class="invalid-feedback">{{ $errors->first('graduation_university') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-4">
                                          <label>Graduation Year</label>
                                          <input type="text" name="graduation_year" value="{{old('graduation_year')}}" min="1900" max="2021" step="1" class="form-control"  placeholder="Enter Graduation year" required>
                                          @if ($errors->has('graduation_year'))
                                              <div class="invalid-feedback">{{ $errors->first('graduation_year') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-4">
                                          <label>Graduation CGPA/Percentage</label>
                                          <input type="text" name="graduation_percentage" value="{{old('graduation_percentage')}}" class="form-control"  placeholder="Enter Graduation CGPA/Percentage" required>
                                          @if ($errors->has('graduation_percentage'))
                                              <div class="invalid-feedback">{{ $errors->first('graduation_percentage') }}</div>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <div class="col-sm-4">
                                          <label>Master Degree Board/University</label>
                                          <input type="text" name="master_degree_university" value="{{old('master_degree_university')}}" class="form-control" placeholder="Master Degree Board/University" required>
                                          @if ($errors->has('master_degree_university'))
                                              <div class="invalid-feedback">{{ $errors->first('master_degree_university') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-4">
                                          <label>Master Degree Year</label>
                                          <input type="number" value="{{old('master_degree_year')}}" min="1900" max="2021" step="1" name="master_degree_year" class="form-control" id="" placeholder="Enter Master Degree Year" required>
                                          @if ($errors->has('master_degree_year'))
                                              <div class="invalid-feedback">{{ $errors->first('master_degree_year') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-4">
                                          <label>Master Degree CGPA/Percentage</label>
                                          <input type="text" name="master_degree_percentage" value="{{old('master_degree_percentage')}}" class="form-control" placeholder="Enter Master Degree CGPA/Percentage" required>
                                          @if ($errors->has('master_degree_percentage'))
                                              <div class="invalid-feedback">{{ $errors->first('master_degree_percentage') }}</div>
                                          @endif
                                      </div>
                                  </div>
                                  
                                  <div class="row">
                                    <div class="col-sm-12"><h2>Work Experience</h2></div>
                                  </div>
                                  <div class="work_experience">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label>Company Name:</label>
                                            <input type="text" name="company_name[]" class="form-control company_name" placeholder="Enter Company Name" required>
                                            @if ($errors->has('company_name'))
                                                <div class="invalid-feedback">{{ $errors->first('company_name') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Designation</label>
                                            <input type="text" name="designation[]" class="form-control designation" placeholder="Enter Designation" required>
                                            @if ($errors->has('designation'))
                                                <div class="invalid-feedback">{{ $errors->first('designation') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-sm-2">
                                            <label>From date</label>
                                            <input type="text" name="from_date[]" class="form-control from_date" placeholder="Enter From date" required>
                                            @if ($errors->has('from_date'))
                                                <div class="invalid-feedback">{{ $errors->first('from_date') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-sm-2">
                                            <label>To date</label>
                                            <input type="text" name="to_date[]" class="form-control to_date" placeholder="Enter To date" required>
                                            @if ($errors->has('to_date'))
                                                <div class="invalid-feedback">{{ $errors->first('to_date') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-md-2">
                                          <label>&nbsp;</label>
                                          <div class="form-group mt-1 add_btn_div">
                                            <a href="javascript:void(0)" class="add_btn"><span class="fa fa-plus-circle"></span></a>
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-sm-12"><h2>Known Languages</h2></div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang" name="lang1" type="checkbox" id="inlineCheckbox1" value="English" data-val="lang1">
                                        <label class="form-check-label" for="inlineCheckbox1">English</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang_data lang_data1" name="lang1_read" type="checkbox" id="inlineCheckbox2" value="read" disabled="disabled" data-val="lang_data1">
                                        <label class="form-check-label" for="inlineCheckbox2">Read</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang_data lang_data1" name="lang1_write" type="checkbox" id="inlineCheckbox3" value="write" disabled="disabled" data-val="lang_data1">
                                        <label class="form-check-label" for="inlineCheckbox3">Write</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang_data lang_data1" name="lang1_speak" type="checkbox" id="inlineCheckbox4" value="speak" disabled="disabled" data-val="lang_data1">
                                        <label class="form-check-label" for="inlineCheckbox4">Speak</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang" name="lang2" type="checkbox" id="inlineCheckbox5" value="Hindi" data-val="lang2">
                                        <label class="form-check-label" for="inlineCheckbox5">Hindi</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang_data lang_data2" name="lang2_read" type="checkbox" id="inlineCheckbox6" value="read" disabled="disabled" data-val="lang_data2">
                                        <label class="form-check-label" for="inlineCheckbox6">Read</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang_data lang_data2" name="lang2_write" type="checkbox" id="inlineCheckbox7" value="write" disabled="disabled" data-val="lang_data2">
                                        <label class="form-check-label" for="inlineCheckbox7">Write</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang_data lang_data2" name="lang2_speak" type="checkbox" id="inlineCheckbox8" value="speak" disabled="disabled" data-val="lang_data2">
                                        <label class="form-check-label" for="inlineCheckbox8">Speak</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang" name="lang3" type="checkbox" id="inlineCheckbox9" value="Gujrati" data-val="lang3">
                                        <label class="form-check-label" for="inlineCheckbox9">Gujrati</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang_data lang_data3" name="lang3_read" type="checkbox" id="inlineCheckbox10" value="read" disabled="disabled" data-val="lang_data3">
                                        <label class="form-check-label" for="inlineCheckbox10">Read</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang_data lang_data3" name="lang3_write" type="checkbox" id="inlineCheckbox11" value="write" disabled="disabled" data-val="lang_data3">
                                        <label class="form-check-label" for="inlineCheckbox11">Write</label>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input lang_data lang_data3" name="lang3_speak" type="checkbox" id="inlineCheckbox12" value="speak" disabled="disabled" data-val="lang_data3">
                                        <label class="form-check-label" for="inlineCheckbox12">Speak</label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                      <div class="col-sm-12"><h2>Technical Experience</h2></div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_exp" name="tech_lang1" type="checkbox" id="inlineCheckbox13" value="PHP" data-val="tech_lang1">
                                          <label class="form-check-label" for="inlineCheckbox13">PHP</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data1" name="tech_lang1_type" type="radio" id="inlineCheckbox14" value="Beginer" disabled="disabled" data-val="tech_data1">
                                          <label class="form-check-label" for="inlineCheckbox14">Beginer</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data1" name="tech_lang1_type" type="radio" id="inlineCheckbox15" value="Mideator" disabled="disabled" data-val="tech_data1">
                                          <label class="form-check-label" for="inlineCheckbox15">Mideator</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data1" name="tech_lang1_type" type="radio" id="inlineCheckbox16" value="Expert" disabled="disabled" data-val="tech_data1">
                                          <label class="form-check-label" for="inlineCheckbox16">Expert</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_exp" name="tech_lang2" type="checkbox" id="inlineCheckbox17" value="Mysql" data-val="tech_lang2">
                                          <label class="form-check-label" for="inlineCheckbox17">Mysql</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data2" name="tech_lang2_type" type="radio" id="inlineCheckbox18" value="Beginer" disabled="disabled" data-val="tech_data2">
                                          <label class="form-check-label" for="inlineCheckbox18">Beginer</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data2" name="tech_lang2_type" type="radio" id="inlineCheckbox19" value="Mideator" disabled="disabled" data-val="tech_data2">
                                          <label class="form-check-label" for="inlineCheckbox19">Mideator</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data2" name="tech_lang2_type" type="radio" id="inlineCheckbox20" value="Expert" disabled="disabled" data-val="tech_data2">
                                          <label class="form-check-label" for="inlineCheckbox20">Expert</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_exp" name="tech_lang3" type="checkbox" id="inlineCheckbox21" value="Laravel" data-val="tech_lang3">
                                          <label class="form-check-label" for="inlineCheckbox21">Laravel</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data3" name="tech_lang3_type" type="radio" id="inlineCheckbox22" value="Beginer" disabled="disabled" data-val="tech_data3">
                                          <label class="form-check-label" for="inlineCheckbox22">Beginer</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data3" name="tech_lang3_type" type="radio" id="inlineCheckbox23" value="Mideator" disabled="disabled" data-val="tech_data3">
                                          <label class="form-check-label" for="inlineCheckbox23">Mideator</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data3" name="tech_lang3_type" type="radio" id="inlineCheckbox24" value="Expert" disabled="disabled" data-val="tech_data3">
                                          <label class="form-check-label" for="inlineCheckbox24">Expert</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_exp" name="tech_lang4" type="checkbox" id="inlineCheckbox25" value="Oracle" data-val="tech_lang4">
                                          <label class="form-check-label" for="inlineCheckbox25">Oracle</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data4" name="tech_lang4_type" type="radio" id="inlineCheckbox26" value="Beginer" disabled="disabled" data-val="tech_data4">
                                          <label class="form-check-label" for="inlineCheckbox26">Beginer</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data4" name="tech_lang4_type" type="radio" id="inlineCheckbox27" value="Mideator" disabled="disabled" data-val="tech_data4">
                                          <label class="form-check-label" for="inlineCheckbox27">Mideator</label>
                                        </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="form-check form-check-inline">
                                          <input class="form-check-input tech_data tech_data4" name="tech_lang4_type" type="radio" id="inlineCheckbox28" value="Expert" disabled="disabled" data-val="tech_data4">
                                          <label class="form-check-label" for="inlineCheckbox28">Expert</label>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <div class="col-sm-12"><h2>Preference</h2></div>
                                      <div class="col-sm-6">
                                          <label>Preferred Location</label>
                                          <select name="preferred_location" class="form-control" required>
                                            <option value="ahmedabad">Ahmedabad</option>
                                            <option value="rajkot">Rajkot</option>
                                            <option value="mumbai">Mumbai</option>
                                          </select>
                                          @if ($errors->has('preferred_location'))
                                              <div class="invalid-feedback">{{ $errors->first('preferred_location') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-6">
                                          <label>Expected CTC</label>
                                          <input type="text" name="expected_ctc" value="{{old('expected_ctc')}}" class="form-control" placeholder="Enter Expected CTC" required>
                                          @if ($errors->has('expected_ctc'))
                                              <div class="invalid-feedback">{{ $errors->first('expected_ctc') }}</div>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <div class="col-sm-6">
                                          <label>Current CTC</label>
                                          <input type="text" name="current_ctc" value="{{old('current_ctc')}}" class="form-control"  placeholder="Enter Current CTC" required>
                                          @if ($errors->has('current_ctc'))
                                              <div class="invalid-feedback">{{ $errors->first('current_ctc') }}</div>
                                          @endif
                                      </div>
                                      <div class="col-sm-6">
                                          <label>Notice Period</label>
                                          <input type="text" name="notice_period" value="{{old('notice_period')}}" class="form-control"  placeholder="Enter Notice Period" required>
                                          @if ($errors->has('notice_period'))
                                              <div class="invalid-feedback">{{ $errors->first('notice_period') }}</div>
                                          @endif
                                      </div>
                                  </div>
                                  <button type="submit" class="btn btn-primary px-4">Save</button>
                              </form>
                          </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- [ Main Content ] end -->
@endsection


@push('js')
<script type="text/javascript">
    $(function(){
        
      //add
      $(document.body).on('click','.add_btn',function(){
          var work_experience = $('.work_experience').find('.row:last').clone();
          var company_name = work_experience.find('.company_name:last').val();
          var designation = work_experience.find('.designation:last').val();
          var from_date = work_experience.find('.from_date:last').val();
          var to_date = work_experience.find('.to_date:last').val();
          console.log(company_name);
          // return false;

          if (typeof(work_experience) == "undefined" || work_experience == null || work_experience == '' || typeof(company_name) == "undefined" || company_name == null || company_name == '' || typeof(designation) == "undefined" || designation == null || designation == '' || typeof(to_date) == "undefined" || to_date == null || to_date == ''){
            alert('Please fill up all fields');
          }else{
            work_experience.find('.company_name:last').val('');
            work_experience.find('.designation:last').val('');
            work_experience.find('.from_date:last').val('');
            work_experience.find('.to_date:last').val('');
            work_experience.find('.add_btn_div:last').html('<a href="javascript:void(0)" class="remove_btn"><span class="fa fa-minus-circle"></span></a>');

            $('.work_experience').append(work_experience);

            work_experience.find('.from_date:last').attr("id", "").removeClass('hasDatepicker')
            .removeData('datepicker')
            .unbind();
            work_experience.find(".to_date:last").attr("id", "").removeClass('hasDatepicker')
            .removeData('datepicker')
            .unbind()

            //start date
            work_experience.find('.from_date:last').datepicker({ 
                dateFormat:'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                // numberOfMonths: 2,
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate());
                    work_experience.find('.to_date:last').datepicker("option", "minDate", dt);
                }
            });

            //end date
            work_experience.find(".to_date:last").datepicker({
                dateFormat:'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                // numberOfMonths: 2,
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate());
                    work_experience.find('.from_date:last').datepicker("option", "maxDate", dt);
                }
            });
          }

      });

      //Remove
      $(document.body).on('click','.remove_btn',function(e){
          e.preventDefault();
          var me = $(this);
          me.closest('.row').remove();
      });

      $(document.body).on('click','.lang',function(e){
        var data_val = $(this).attr('data-val');
        var read_chk = data_val+'_read';
        var write_chk = data_val+'_write';
        var speak_chk = data_val+'_speak';
          if($(this).prop("checked") == true){  
          console.log('checked');            
              $('input[name="'+read_chk+'"]').attr("disabled", false);
              $('input[name="'+write_chk+'"]').attr("disabled", false);
              $('input[name="'+speak_chk+'"]').attr("disabled", false);

              $('input[name="'+read_chk+'"]').prop('required',true);
              $('input[name="'+write_chk+'"]').prop('required',true);
              $('input[name="'+speak_chk+'"]').prop('required',true);
          }else if($(this).prop("checked") == false){
              $('input[name="'+read_chk+'"]').attr("disabled", true);
              $('input[name="'+write_chk+'"]').attr("disabled", true);
              $('input[name="'+speak_chk+'"]').attr("disabled", true);

              $('input[name="'+read_chk+'"]').prop('checked', false);
              $('input[name="'+write_chk+'"]').prop('checked', false);
              $('input[name="'+speak_chk+'"]').prop('checked', false);

               $('input[name="'+read_chk+'"]').prop('required',false);
              $('input[name="'+write_chk+'"]').prop('required',false);
              $('input[name="'+speak_chk+'"]').prop('required',false);
          }
      })

      $(document.body).on('click','.tech_exp',function(e){
        var data_val = $(this).attr('data-val');
        var tech_chk = data_val+'_type';
          if($(this).prop("checked") == true){  
          console.log('checked');            
              $('input[name="'+tech_chk+'"]').attr("disabled", false);
              $('input[name="'+tech_chk+'"]').prop('required',true);
          }else if($(this).prop("checked") == false){
              $('input[name="'+tech_chk+'"]').attr("disabled", true);
               $('input[name="'+tech_chk+'"]').prop('required',false);
              
              $('input[name="'+tech_chk+'"]').prop('checked', false);
          }
      })

      $(document.body).on('click','.lang_data',function(e){
        var data_val = $(this).attr('data-val');
        if($(this).prop("checked") == true){  
              $('.'+data_val).prop('required',false);
          }
      });

      $(document.body).on('click','.tech_data',function(e){
        var data_val = $(this).attr('data-val');
        if($(this).prop("checked") == true){  
              $('.'+data_val).prop('required',false);
          }
      });
        
    });
</script>
@endpush
