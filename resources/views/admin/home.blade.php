@extends('admin.layout.app')

@section('content')
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->

                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <div class="row">
                            
                            

                            <!-- Admin -->
                            <div class="col-md-6 col-xl-6">
                                <div class="card card-social">
                                    <div class="card-block border-bottom">
                                        <div class="row align-items-center justify-content-center">
                                            <div class="col-auto">
                                                <i class="feather icon-user text-c-blue f-36"></i>
                                            </div>
                                            <div class="col text-right">
                                                <h3>Application</h3>
                                                <h5 class="text-c-blue mb-0">{{App\Models\Application::count()}} <span class="text-muted">Total Application</span></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- [ Main Content ] end -->
@endsection


@push('js')
<script type="text/javascript">
    $(function(){
        $('.dashboard').addClass('active');
    });
</script>
@endpush