@extends('admin.layout.app')

@section('content')
<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Application</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0)">Applications</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ Hover-table ] start -->
                            <div class="col-xl-12">

                                @include('admin.layout.session')

                                <div class="card">
                                    <div class="card-header">
                                        <h5>Admins</h5>

                                        <div class="d-inline-block dropdown float-right">
                                            @can('add_applications')
                                                <a href="{{ route('admin.applications.create') }}" class="btn btn-glow-primary btn-primary btn-sm"><i class="feather icon-plus-circle"></i>Add New User</a>
                                            @endcan
                                            
                                        </div>
                                    </div>
                                    <div class="card-block table-border-style">
                                        <div class="table-responsive Recent-Users">
                                            <table class="table table-hover application_datatable">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Address</th>
                                                        @if(auth()->user()->can('edit_applications') || auth()->user()->can('delete_applications'))
                                                          <th>Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- [ Hover-table ] end -->

                            
                        </div>
                        <!-- [ Main Content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- [ Main Content ] end -->
@endsection

@push('js')
<script type="text/javascript">
$(function(){
        $('.applications').addClass('active');

    var oTable = $('.application_datatable').DataTable({
        processing: false,
        serverSide: true,
        ajax:'{!! route('admin.applications.datatable') !!}',
        columns: [
            {data: 'name', name: 'name', sortable: true, orderable: true, searchable: true},
            {data: 'email', name: 'email', sortable: true, orderable: true, searchable: true},
            {data: 'address', name: 'address', sortable: true, orderable: true, searchable: true},
            @if(auth()->user()->can('edit_applications') || auth()->user()->can('delete_applications'))
            {data: 'action', sortable: false,searchable: false},
            @endif
        ],

        @if(!auth()->user()->can('edit_applications') && !auth()->user()->can('delete_applications'))
            order: [[1, 'desc']],
        @elseif(auth()->user()->can('edit_applications') && auth()->user()->can('delete_applications'))
            order: [[2, 'desc']],
        @elseif(auth()->user()->can('edit_applications'))
            order: [[1, 'desc']],
        @elseif(auth()->user()->can('delete_applications'))
            order: [[2, 'desc']],
        @endif
        "pagingType": "full_numbers",
        "language": {
            // "emptyTable": "No fees",
            "search": '<span>Filter:</span> _INPUT_',
            "lengthMenu": '<span>Show:</span> _MENU_',
            "searchPlaceholder": "Search..."
            // "paginate": { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        autoWidth: false,
        'fnDrawCallback': function( oSettings ) {
        }
    });


});
</script>
@endpush