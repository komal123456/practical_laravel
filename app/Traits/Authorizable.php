<?php
namespace App\Traits;

trait Authorizable
{
    private $abilities = [
        'index' => 'view',
        'edit' => 'edit',
        'show' => 'view',
        'update' => 'edit',
        'create' => 'add',
        'store' => 'add',
        'destroy' => 'delete'
    ];

    /**
     * Override of callAction to perform the authorization before
     *
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function callAction($method, $parameters)
    {
    	// echo '<pre>';
    	// print_r($method);
    	// exit;
        if( $ability = $this->getAbility($method) ) {
            // echo $ability;exit;
            // print_r($this);exit;
            //echo '<pre>';print_r($this->User->getAllPermissions());exit;
            $this->authorize($ability);
            //echo 'jiii';exit;
        }
        
        return parent::callAction($method, $parameters);
    }

    public function getAbility($method)
    {
    	// echo 'hi';exit;
        $routeName = explode('.', \Request::route()->getName());
        $action = array_get($this->getAbilities(), $method);
        // echo $action;
        // echo $action ? $action . '_' . $routeName[1] : null;exit;
        // print_r($routeName);
        // exit;
        return $action ? $action . '_' . $routeName[1] : null;
    }

    private function getAbilities()
    {
        return $this->abilities;
    }

    public function setAbilities($abilities)
    {
        $this->abilities = $abilities;
    }
}

