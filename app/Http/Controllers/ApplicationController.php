<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Application;
use App\Models\WorkExperience;

use Redirect;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('application');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|string|unique:applications',    		
            'address' => 'required',
            'gender' => 'required',
            'ssc_university' => 'required',
            'ssc_year' => 'required',
            'ssc_percentage' => 'required',
            'hsc_university' => 'required',
            'hsc_year' => 'required',
            'hsc_percentage' => 'required',
            'graduation_university' => 'required',
            'graduation_year' => 'required',
            'graduation_percentage' => 'required',
            'master_degree_university' => 'required',
            'master_degree_year' => 'required',
            'master_degree_percentage' => 'required',
            'preferred_location' => 'required',
            'expected_ctc' => 'required',
            'current_ctc' => 'required',
            'notice_period' => 'required',
        ]);

        $input = $request->except(['_token']);        
        $application = Application::Create($input);

        $company_name_array = $request->company_name;
        $designation_array = $request->designation;
        $from_date_array = $request->from_date;
        $to_date_array = $request->to_date;

        if(!empty($company_name_array)){
            foreach ($company_name_array as $key => $price) {
                //collect all inserted record IDs
                $single_array = ['application_id' => $application->id, 'company_name' => $company_name_array[$key], 'designation' => $designation_array[$key], 'from_date' => $from_date_array[$key], 'to_date' => $to_date_array[$key]]; 
                WorkExperience::Create($single_array);
            }
        }

        return Redirect::route('applications.index')->with('success', 'Application create successfully.');
        // dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
