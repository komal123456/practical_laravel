<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Application;
use App\Models\WorkExperience;

use DataTables;
use Response;
use Mail;
use Excel;
use Redirect;
use Image;
use File;
use Storage;

use App\Traits\Authorizable;

class ApplicationController extends Controller
{

    use Authorizable;
    
    public function __construct() {
        $this->middleware('auth:admin');
        //$this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.application.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $application = Application::find($id);
        if(empty($application)){
            return Redirect::route('admin.applications.index')->with('error','Application not found.');
        }
        return view('admin.application.edit',compact('application'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Application $application)
    {
        // $this->validate($request, [
        //     'name' => 'required|unique:blog_categories,name,'.$blog_category->id,            
        // ]);

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|string|unique:applications,email,'.$application->id,    		
            'address' => 'required',
            'gender' => 'required',
            'ssc_university' => 'required',
            'ssc_year' => 'required',
            'ssc_percentage' => 'required',
            'hsc_university' => 'required',
            'hsc_year' => 'required',
            'hsc_percentage' => 'required',
            'graduation_university' => 'required',
            'graduation_year' => 'required',
            'graduation_percentage' => 'required',
            'master_degree_university' => 'required',
            'master_degree_year' => 'required',
            'master_degree_percentage' => 'required',
            'preferred_location' => 'required',
            'expected_ctc' => 'required',
            'current_ctc' => 'required',
            'notice_period' => 'required',
        ]);

        $input = $request->except(['_token','_method']);
        $input['lang1'] = (isset($request->lang1)) ? $request->lang1 : '';
        $input['lang1_read'] = (isset($request->lang1_read)) ? $request->lang1_read : '';
        $input['lang1_write'] = (isset($request->lang1_write)) ? $request->lang1_write : '';
        $input['lang1_speak'] = (isset($request->lang1_speak)) ? $request->lang1_speak : '';
        $input['lang2'] = (isset($request->lang2)) ? $request->lang2 : '';
        $input['lang2_read'] = (isset($request->lang2_read)) ? $request->lang2_read : '';
        $input['lang2_write'] = (isset($request->lang2_write)) ? $request->lang2_write : '';
        $input['lang2_speak'] = (isset($request->lang2_speak)) ? $request->lang2_speak : '';
        $input['lang3'] = (isset($request->lang3)) ? $request->lang3 : '';
        $input['lang3_read'] = (isset($request->lang3_read)) ? $request->lang3_read : '';
        $input['lang3_write'] = (isset($request->lang3_write)) ? $request->lang3_write : '';
        $input['lang3_speak'] = (isset($request->lang3_speak)) ? $request->lang3_speak : '';

        $input['tech_lang1'] = (isset($request->tech_lang1)) ? $request->tech_lang1 : '';
        $input['tech_lang1_type'] = (isset($request->tech_lang1_type)) ? $request->tech_lang1_type : '';
        $input['tech_lang2'] = (isset($request->tech_lang2)) ? $request->tech_lang2 : '';
        $input['tech_lang2_type'] = (isset($request->tech_lang2_type)) ? $request->tech_lang2_type : '';
        $input['tech_lang3'] = (isset($request->tech_lang3)) ? $request->tech_lang3 : '';
        $input['tech_lang3_type'] = (isset($request->tech_lang3_type)) ? $request->tech_lang3_type : '';
        $input['tech_lang4'] = (isset($request->tech_lang4)) ? $request->tech_lang4 : '';
        $input['tech_lang4_type'] = (isset($request->tech_lang4_type)) ? $request->tech_lang4_type : '';

        $application->update($input);

        $company_name_array = $request->company_name;
        $designation_array = $request->designation;
        $from_date_array = $request->from_date;
        $to_date_array = $request->to_date;

        WorkExperience::where('application_id', '=', $application->id)->delete();
        if(!empty($company_name_array)){
            foreach ($company_name_array as $key => $price) {
                //collect all inserted record IDs
                $single_array = ['application_id' => $application->id, 'company_name' => $company_name_array[$key], 'designation' => $designation_array[$key], 'from_date' => $from_date_array[$key], 'to_date' => $to_date_array[$key]]; 
                WorkExperience::Create($single_array);
            }
        }

        return Redirect::route('admin.applications.index')->with('success','Application update successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        $Application = Application::find($id);
        if(!empty($Application)){
            $Application->delete();
        }
        
        if ($request->ajax()) {
            return response()->json(['status' => 'success', 'msg' => "Application deleted successfully."]);
        }
        return Redirect::route('admin.applications.index')->with('success','Application deleted successfully.');
    }

    public function Datatable(Request $request) {
        $raw = Application::query();
        $datatable = app('datatables')->of($raw)

        ->editColumn('created_at', function ($raw) {
            if($raw->created_at == ''){
                return '-';
            }else{
                return date('Y-m-d H:i:s A', strtotime($raw->created_at));
            }
        })

        ->addColumn('action', function ($raw) {
            $html = '';
            if(auth()->user()->can('edit_applications')){
                $html .= '<a href="'.route("admin.applications.edit", $raw->id).'" class="label theme-bg2 text-white f-12">Edit</a>  ';
            }
            if(auth()->user()->can('delete_applications')){
                $html .= '<a class="label theme-bg text-white f-12 delete-application-btn" href="javascript:void(0)" data-url="'. route("admin.applications.destroy", $raw->id) .'">Delete</a>';
            }
            return $html;
        });
        
        return $datatable->make(true);

    }

}
