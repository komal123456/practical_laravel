<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'email', 'address', 'gender', 'ssc_university', 'ssc_year', 'ssc_percentage', 'hsc_university', 'hsc_year', 'hsc_percentage', 'graduation_university', 'graduation_year', 'graduation_percentage', 'master_degree_university', 'master_degree_year', 'master_degree_percentage', 'lang1', 'lang1_read', 'lang1_write', 'lang1_speak', 'lang2', 'lang2_read', 'lang2_write', 'lang2_speak', 'lang3', 'lang3_read', 'lang3_write', 'lang3_speak', 'tech_lang1', 'tech_lang1_type', 'tech_lang2', 'tech_lang2_type', 'tech_lang3', 'tech_lang3_type', 'tech_lang4', 'tech_lang4_type', 'preferred_location', 'expected_ctc', 'current_ctc', 'notice_period'
    ];

    public function work_experiences() {
        return $this->hasMany('App\Models\WorkExperience');
    }
}
