<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission
{
    public static function defaultPermissions()
	{
	    return [
	        
	        'view_admins',
	        'add_admins',
	        'edit_admins',
			'delete_admins',

	        'view_applications',
	        'add_applications',
	        'edit_applications',
	        'delete_applications',

	    ];
	}
}
