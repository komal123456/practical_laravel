<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkExperience extends Model
{
    use HasFactory;

    protected $fillable = [
        'application_id', 'company_name', 'designation', 'from_date', 'to_date'
    ];

    public function application()
    {
        return $this->belongsTo('App\Model\Application');
    }
}
