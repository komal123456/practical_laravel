<?php

// Route::get('/home', function () {
//     $users[] = Auth::user();
//     $users[] = Auth::guard()->user();
//     $users[] = Auth::guard('admin')->user();

//     //dd($users);

//     return view('admin.home');
// })->name('home');


Route::group(['middleware' => ['admin']], function() {

	Route::namespace('backend')->group(function () {

        Route::get('/home', function () {
            $users[] = Auth::user();
            $users[] = Auth::guard()->user();
            $users[] = Auth::guard('admin')->user();

            //dd($users);

            return view('admin.home');
        })->name('home');
		// Route::get('home', 'HomeController@index')->name('home');

        Route::get('applications-datatable','ApplicationController@Datatable')->name('applications.datatable');
	    Route::resource('applications', 'ApplicationController');
		
    });

});

