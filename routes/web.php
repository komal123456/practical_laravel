<?php

// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('application');
    // return redirect('/home');
});

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['namespace'=>'App\Http\Controllers'], function () {
	Route::resource('applications', 'ApplicationController');
});
Route::group(['prefix' => 'admin', 'namespace'=>'App\Http\Controllers\backend\AdminAuth'], function () {
  Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'LoginController@login');
  Route::post('/logout', 'LoginController@logout')->name('admin.logout');
  Route::get('/logout', 'LoginController@logout')->name('admin.logout');

  // Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  // Route::post('/register', 'AdminAuth\RegisterController@register');

  // Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  // Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  // Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  // Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
