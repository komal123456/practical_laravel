$(document).ready(function(){

    var token = $('meta[name="csrf-token"]').attr('content');

    $('.select2').each(function () {
        $(this).select2({
          // theme: 'bootstrap4',
          // width: 'style',
          placeholder: $(this).attr('placeholder'),
          allowClear: Boolean($(this).data('allow-clear')),
        });
    });

    //start date
    $( ".from_date" ).datepicker({ 
        dateFormat:'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        // numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate());
            $(".to_date").datepicker("option", "minDate", dt);
        }
    });

    //end date
    $(".to_date").datepicker({
        dateFormat:'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        // numberOfMonths: 2,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate());
            $(".from_date").datepicker("option", "maxDate", dt);
        }
    });




// ---------------------------------------------Application-----------------------------------------------
$(".application_form").validate({
    //ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
    // eslint-disable-next-line object-shorthand
    // errorElement: 'em',
    highlight: function highlight(element) {
        $(element).addClass('is-invalid').removeClass('is-valid');
    },
    // eslint-disable-next-line object-shorthand
    unhighlight: function unhighlight(element) {
        $(element).addClass('is-valid').removeClass('is-invalid');
    },

    // Different components require proper error label placement
    errorPlacement: function errorPlacement(error, element) {
        error.addClass('invalid-feedback');

        if (element.prop('type') === 'checkbox') {
            error.insertAfter(element.parent('label'));
        } else {
            error.insertAfter(element);
        }
    },
    validClass: "validation-valid-label",
    success: function(label,element) {
       // label.addClass("validation-valid-label").text("Successfully")
        //label.parent().removeClass('error');
        $(element).closest('.form-group').removeClass('has-error');
        label.remove();
      // console.log(label);
    },
    submitHandler: function(form) {
        form.submit();
    },
    rules: {
        // category_name: {
        //     minlength: 2,
        //     maxlength: 100,
        // },
        // cat_image:{
        //     fileType: {
        //         types: ["jpg", "jpeg", "png"]
        //     },
        //     maxFileSize: {
        //         "unit": "MB",
        //         "size": 5
        //     },
        //     minFileSize: {
        //         "unit": "KB",
        //         "size": 10
        //     }
        // }
    },
    messages: {
        
    }
});

$(document.body).on('click','.delete-application-btn',function(e){
    e.preventDefault();
    var me = $(this);
    var first_messages = 'Are you sure you want to delete this Application? This process is not reversable.';
    var final_messages = 'Are you sure ?';
    var datatable_name = '.application_datatable';
    DeleteConfirmAlert(me, first_messages, final_messages, datatable_name);
});



jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid Email.");
    





}); //end document.ready

//----------------------------------------All Function---------------------------------------------------
function DeleteConfirmAlert(me, first_messages, final_messages, datatable_name){
    $.confirm({
      title: 'Confirmation',
      content: first_messages,
      icon: 'fa fa-exclamation-triangle',
      animation: 'scale',
      closeAnimation: 'scale',
      opacity: 0.5,
      buttons: {
          'confirm': {
              text: 'Proceed',
              btnClass: 'btn-blue',
              action: function(){
                  $.confirm({
                      title: 'This maybe critical',
                      content: final_messages,
                      icon: 'fa skull-crossbones',
                      animation: 'scale',
                      closeAnimation: 'zoom',
                      buttons: {
                          confirm: {
                              text: 'Yes, sure!',
                              btnClass: 'btn-danger',
                              action: function(){
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                var url = me.data('url');
                                $.ajax({
                                    url: url,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    data: {method: '_DELETE', submit: true},
                                    success: function(data){
                                        var session = data.msg;
                                        if(data.status == 'success'){
                                            $(datatable_name).DataTable().draw(false);
                                            if (session != ""){
                                                Swal.fire({
                                                    title: 'Good job!',
                                                    text: session,
                                                    type: 'success',
                                                    // confirmButtonText: 'Deposit Funds'
                                                }).then(function() {
                                                    // window.location = "https://cryptolico.com/portal/deposit-fund";
                                                });
                                            }
                                        }else{
                                            if (session != ""){
                                                Swal.fire({
                                                    title: 'Sorry!',
                                                    text: session,
                                                    type: 'warning',
                                                })
                                            }
                                        }
                                        
                                    }
                                })
                              }
                          },
                          cancel: function(){
                              //$.alert('you clicked on <strong>cancel</strong>');
                          }
                      }
                  });
              }
          },
          cancel: function(){
              //$.alert('you clicked on <strong>cancel</strong>');
          },
          
      }
    });

} 



function SimpleconfirmAlert() {
    var def = $.Deferred();
    $.confirm({
        title: 'Confirmation',
        content: 'Are You Sure?',
        icon: 'fa fa-exclamation-triangle',
        animation: 'scale',
        closeAnimation: 'scale',
        opacity: 0.5,
        buttons: {
            confirm: {
                text: 'Proceed',
                btnClass: 'btn-blue',
                action: function () {
                    // return true;
                    def.resolve("Yes");
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn btn-danger',
                action:  function () {
                    // return false;
                    def.resolve("No");
                }
            }
        }
    });
    
    return def.promise();

}

function SimpleAlert(message){
    $.alert({
        title: 'Error',
        icon: 'fa skull-crossbones',
        type: 'red',
        content: message,
    });
}

function showLoader(){
    $('body').loadingModal({text: 'Loading...'});
    var delay = function (ms) {
        return new Promise(function (r) {
            setTimeout(r, ms)
        })
    };
    $('body').loadingModal('animation', 'circle').loadingModal('backgroundColor', 'black');
    return delay(2000);
}

function hideLoader(){
    // hide the loading modal
    // $('body').loadingModal('hide');
    // destroy the plugin
    $('body').loadingModal('destroy');

}


