<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Permission;
use App\Models\Role;
use App\Models\Admin;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        if ($this->command->confirm('Do you wish to refresh migration before seeding, it will clear all old data ?')) {

            // Call the php artisan migrate:refresh
            $this->command->call('migrate:refresh');
            $this->command->warn("Data cleared, starting from blank database.");

            $this->call(AdminSeeder::class);
            $this->call(RoleSeeder::class);
            
        }

        // Seed the default permissions
        $permissions = Permission::defaultPermissions();
        foreach ($permissions as $perms) {
            Permission::firstOrCreate(['name' => trim($perms), 'guard_name' => 'admin']);
        }
        $this->command->info('Default Permissions added.');

        // add roles
        $roles_array = Role::where('guard_name', 'admin')->get();
        foreach($roles_array as $role) {
            $this->command->info($role->name);
            if( $role->name == 'admin' ) {
                // assign all permissions
                $role->syncPermissions(Permission::where('name', '=', 'view_applications')
                    ->orWhere('name', '=', 'edit_applications')
                    ->orWhere('name', '=', 'delete_applications')
                    ->get());
                $this->command->info('Admin granted all the permissions');
            }
            // create one user for each role
            $this->createUser($role);
        }

    }

    private function createUser($role)
    {
        
        if( $role->name == 'admin' ) {
            $admin = Admin::find(1);
            $admin->assignRole($role->name);
            $this->command->info('Here is your admin details to login:');
            $this->command->warn('Email is: ' .$admin->email);
            $this->command->warn('Password is: "123456"');
        }
    }

}
