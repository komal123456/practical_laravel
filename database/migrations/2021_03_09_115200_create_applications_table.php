<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable()->unique();
            $table->string('address')->nullable();
            $table->string('gender')->nullable();
            $table->string('ssc_university')->nullable();
            $table->string('ssc_year')->nullable();
            $table->string('ssc_percentage')->nullable();
            $table->string('hsc_university')->nullable();
            $table->string('hsc_year')->nullable();
            $table->string('hsc_percentage')->nullable();
            $table->string('graduation_university')->nullable();
            $table->string('graduation_year')->nullable();
            $table->string('graduation_percentage')->nullable();
            $table->string('master_degree_university')->nullable();
            $table->string('master_degree_year')->nullable();
            $table->string('master_degree_percentage')->nullable();
            $table->string('lang1')->nullable();
            $table->string('lang1_read')->nullable();
            $table->string('lang1_write')->nullable();
            $table->string('lang1_speak')->nullable();
            $table->string('lang2')->nullable();
            $table->string('lang2_read')->nullable();
            $table->string('lang2_write')->nullable();
            $table->string('lang2_speak')->nullable();
            $table->string('lang3')->nullable();
            $table->string('lang3_read')->nullable();
            $table->string('lang3_write')->nullable();
            $table->string('lang3_speak')->nullable();
            $table->string('tech_lang1')->nullable();
            $table->string('tech_lang1_type')->nullable();
            $table->string('tech_lang2')->nullable();
            $table->string('tech_lang2_type')->nullable();
            $table->string('tech_lang3')->nullable();
            $table->string('tech_lang3_type')->nullable();
            $table->string('tech_lang4')->nullable();
            $table->string('tech_lang4_type')->nullable();
            $table->string('preferred_location')->nullable();
            $table->string('expected_ctc')->nullable();
            $table->string('current_ctc')->nullable();
            $table->string('notice_period')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
